# Intelimétrica Test API

## Getting Started

To get started, install all dependencies:

- Install with `npm i` or `npm install`

## Initialize DataBase

Before using this API for the first time you must execute an init file, but firts:

Don't forget to fill enviroment variables values

```env
HOST = YOUR_HOST
PORT = RUNNING_PORT
DB_HOST = DATABASE_HOST_PROVIDER
DB_NAME = DATABASE_NAME
DB_PASS = DATABASE_PASSWORD
DB_USER = DATABASE_USER
```

Execute `dbInit` script

```console
npm run dbInit
```

## Initialize Manually DataBase

If you want to run queries manually, you can find it at `database/db.sql`


## Development Mode

Execute `dev` script

```console
npm run dev
```

## Production Mode

Execute `start` script

```console
npm start
```

## List of Endpoints

- **/api/restaurants**
  - GET
- **/api/restaurants/statistics**
  - GET  
- **/api/restaurant**
  - POST
- **/api/restaurant/:id**
  - GET , PUT, DELETE

## Use of Endpoints

### /api/restaurants

- GET

Response example:

```console
[
    {
        "id": "030eaf75-da6e-4748-9727-f2704f831498",
        "rating": 2,
        "name": "Niño - Negrete",
        "site": "https://elizabeth.gob.mx",
        "email": "Luz_Sevilla@gmail.com",
        "phone": "5178-668-409",
        "street": "3041 Gael Torrente",
        "city": "Querétaro Saratown",
        "state": "Oaxaca",
        "lat": 19.4417,
        "lng": -99.1266
    },

    .

    .

    .

    {
        "id": "1ce2e219-d0e5-4f88-85f9-c152485ec7ab",
        "rating": 0,
        "name": "Matos S.L.",
        "site": "https://ricardo.info",
        "email": "JosEmilio.Cotto69@corpfolder.com",
        "phone": "5502-208-117",
        "street": "69982 Luis Gabino Terrenos",
        "city": "Cancún Estefaníahaven",
        "state": "Hidalgo",
        "lat": 19.442,
        "lng": -99.1314
    }
]
```

### /api/restaurants/statistics

- GET

Query parameters:

- `latitude` : Float
- `longitude` : Float
- `radius` : Float

Response example:

```console
{
    "count": 105,
    "avg": 1.8285714285714285,
    "std": 1.4373066270410277
}
```

### /api/restaurant

- POST

Body parameters:

- `rating` : Integer
  - Only allow numbers from 0 to 4
- `name` : Text
- `site` : Text
- `email` : Text
- `phone` : Text
- `street` : Text
- `city` : Text
- `state` : Text
- `lat` : Float
- `lng` : Float

Response example:

```console
{
    "data": {
        "id": "c8537be6-187f-4c5c-8ac0-e77a67378382",
        "rating": "3",
        "name": "Something",
        "site": "http://www.something.com",
        "email": "restaurant@something.com",
        "phone": "6641232234",
        "street": "81414 Luz Ferrocarril",
        "city": "Paolabury",
        "state": "Michoacan",
        "lat": "19.439351178762",
        "lng": "-99.1330661270515"
    }
}
```

### /api/restaurant/:id

`id` must be an uuid identifier

- GET

Response example:

```console
[
    {
        "rating": 0,
        "name": "Matos S.L.",
        "site": "https://ricardo.info",
        "email": "JosEmilio.Cotto69@corpfolder.com",
        "phone": "5502-208-117",
        "street": "69982 Luis Gabino Terrenos",
        "city": "Cancún Estefaníahaven",
        "state": "Hidalgo",
        "lat": 19.442,
        "lng": -99.1314
    }
]
```

- PUT

Response example:

```console
{
    "message": "(Rows matched: 1  Changed: 1  Warnings: 0",
    "data": {
        "rating": "4",
        "name": "Barela e Hijos",
        "site": "https://timoteo.gob.mx",
        "email": "Soledad.Brito7@yahoo.com",
        "phone": "500 281 343",
        "street": "8758 Alondra Romina Parcela",
        "city": "Tijuana",
        "state": "Baja California",
        "lat": "19.4393",
        "lng": "-99.1330"
    }
}
```

- DELETE

Response example:

```console
{
    "fieldCount": 0,
    "affectedRows": 1,
    "insertId": 0,
    "serverStatus": 2,
    "warningCount": 0,
    "message": "",
    "protocol41": true,
    "changedRows": 0
}
```

## Links to test API in production

http://54.71.225.111

https://inteli-test.herokuapp.com