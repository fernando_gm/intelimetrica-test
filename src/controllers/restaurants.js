import db from '../database';
import { v4 as uuidv4 } from 'uuid';

export default {
    getRestaurants: async (req, res, next) => {
        try {
            const cn = db();

            const { limit, offset } = req.query;

            let query = '';

            if (!limit && !offset) {
                query = `SELECT * FROM restaurants`;
            } else {
                query = `SELECT * FROM restaurants LIMIT ${limit} OFFSET ${offset}`;
            }

            cn.query(query, (error, results, fields) => {
                if(error) {
                    console.log(error);
                }
                return res.status(200).json(results);
            });
            
        } catch (e) {
            res.status(500).send({
                message: e.message
            });
            next(e);
        }
    },
    getRestaurant: async (req, res, next) => {
        try {
            const { id } = req.params;

            if (!id) {
                return res.status(404).send({
                    message: 'No ID found in query'
                })
            }
            const cn = db();
            
            let query = `SELECT rating, name, site, email, phone, street, city, state, lat, lng FROM restaurants WHERE id = '${id}'`;

            cn.query(query, (error, results, fields) => {
                if(error) {
                    console.log(error);
                }
                return res.status(200).json(results);
            });
            
        } catch (e) {
            res.status(500).send({
                message: e.message
            });
            next(e);
        }
    },
    getStatistics: async (req, res, next) => {
        try {
            let { latitude, longitude, radius } = req.query;

            if(!latitude && latitude !== 0 && !longitude && longitude !== 0 && !radius) {
                return res.status(400).send({
                    message: `Missing parameters`
                });
            }

            latitude = parseInt(latitude);
            longitude = parseInt(longitude);
            radius = parseInt(radius);

            const minX = latitude - radius;
            const maxX = latitude + radius;

            const minY = longitude - radius;
            const maxY = longitude + radius;

            const cn = db();

            const query = `SELECT rating FROM restaurants WHERE lat >= ${minX} AND lat <= ${maxX} AND lng >= ${minY} AND lng <= ${maxY}`;

            cn.query(query, (error, result, fields) => {
                if(error) {
                    console.log(error);
                }

                const count = result.length;
                let avg = 0;

                result.forEach(item => {
                    avg += item.rating;
                });

                avg = avg / count;

                let std = 0;

                result.forEach(item => {
                    let distance = item.rating - avg;
                    let squared = distance * distance;
                    std += squared;
                });

                std = Math.sqrt(std / count);

                return res.status(200).json({
                    count,
                    avg,
                    std
                });
            });
            
        } catch (e) {
            res.status(500).send({
                message: e.message
            });
            next(e);
        }
    },
    createRestaurant: async (req, res, next) => {
        try {
            let { rating, name, site, email, phone, street,
            city, state, lat, lng } = req.body;

            const id = uuidv4();

            if (!name) {
                return res.status(400).send({
                    message: `parameter 'name' was not received`
                });
            }

            if (parseInt(rating) < 0 || parseInt(rating) > 4 ) {
                return res.status(400).send({
                    message: `rating must be a number between 0 ad 4`
                });
            }

            name = name.replace("'", "\\'");
            street = street.replace("'", "\\'");
            city = city.replace("'", "\\'");
            state = state.replace("'", "\\'");

            const query = `INSERT INTO restaurants (id, rating, name, site, email, phone, street, city, state, lat, lng ) VALUES 
            ('${id}', ${rating ? rating : 0}, '${name}', '${site ? site : ''}', '${email ? email : ''}', '${phone ? phone : ''}', '${street ? street : ''}', '${city ? city : ''}', '${state ? state : ''}', ${lat ? lat : 0}, ${lng ? lng : 0})
            `;

            const cn = db();

            cn.query(query, (error, result, fields) => {
                if(error) {
                    console.log(error);
                }
                return res.status(200).json({
                    data: {
                        id,
                        rating, 
                        name, 
                        site, 
                        email, 
                        phone, 
                        street, 
                        city, 
                        state, 
                        lat, 
                        lng
                    }
                });
            });

        } catch (e) {
            res.status(500).send({
                message: e.message
            });
            next(e);
        }
    },
    updateRestaurant: async (req, res, next) => {
        try {
            const { id } = req.params;

            let { rating, name, site, email, phone, street,
            city, state, lat, lng } = req.body;

            if (!id) {
                return res.status(404).send({
                    message: 'No ID found in query'
                })
            }

            if (!name) {
                return res.status(400).send({
                    message: `parameter 'name' was not received`
                });
            }

            if (parseInt(rating) < 0 || parseInt(rating) > 4 ) {
                return res.status(400).send({
                    message: `rating must be a number between 0 ad 4`
                });
            }

            name = name.replace("'", "\\'");
            street = street.replace("'", "\\'");
            city = city.replace("'", "\\'");
            state = state.replace("'", "\\'");
            
            const cn = db();
            
            let query = `SELECT COUNT(*) FROM restaurants WHERE id = '${id}'`;

            cn.query(query, (error, result, fields) => {
                if(error) {
                    console.log(error);
                }
                
                const count = result[0]['COUNT(*)']

                if (count == 1) {

                    query = `UPDATE restaurants SET
                    rating=${rating ? rating : 0}, name='${name}', site='${site ? site : ''}', email='${email ? email : ''}', phone='${phone ? phone : ''}', 
                    street='${street ? street : ''}', city='${city ? city : ''}', state='${state ? state : ''}', lat=${lat ? lat : 0}, lng=${lng ? lng : 0} 
                    WHERE id = '${id}';
                    `;

                    cn.query(query, (error, result, fields) => {
                        if(error) {
                            console.log(error);
                        }
                        return res.status(200).json({
                            message: result.message,
                            data: {
                                rating, 
                                name, 
                                site, 
                                email, 
                                phone, 
                                street, 
                                city, 
                                state, 
                                lat, 
                                lng
                            }
                        });
                    });

                } else {

                    return res.status(404).send({
                        message: `There is no restaurant id '${id}'`
                    });

                }

                
            });
            
        } catch (e) {
            res.status(500).send({
                message: e.message
            });
            next(e);
        }
    },
    deleteRestaurant: async (req, res, next) => {
        try {

            const { id } = req.params;

            if (!id) {
                return res.status(404).send({
                    message: 'No ID found in query'
                })
            }

            const cn = db();

            let query = `DELETE FROM restaurants WHERE id = '${id}'`;

            cn.query(query, (error, result, fields) => {
                if(error) {
                    console.log(error);
                }
                return res.status(200).json(result);
            });

            
        } catch (e) {
            res.status(500).send({
                message: e.message
            });
            next(e);
        }
    },
}