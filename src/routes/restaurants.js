import routerx from 'express-promise-router';
import restaurantController from '../controllers/restaurants';

const router = routerx();

router.route('/restaurants/')
.get(restaurantController.getRestaurants)

router.route('/restaurants/statistics/')
.get(restaurantController.getStatistics);

router.route('/restaurant/')
.post(restaurantController.createRestaurant);

router.route('/restaurant/:id')
.get(restaurantController.getRestaurant)
.put(restaurantController.updateRestaurant)
.delete(restaurantController.deleteRestaurant);

export default router;