require('dotenv/config');
const csv = require('fast-csv');
const path = require('path');
const fs = require('fs');
const mysql = require('mysql');
const { json } = require('body-parser');

// This file must be executed only the first time

const database = {
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS
};

const cn = mysql.createConnection(database);

const CREATE_TABLE_QUERY = `
CREATE TABLE IF NOT EXISTS restaurants (
    id VARCHAR(40) PRIMARY KEY,
    rating INT DEFAULT 0,
    name TEXT NOT NULL,
    site TEXT,
    email TEXT,
    phone TEXT,
    street TEXT,
    city TEXT,
    state TEXT,
    lat FLOAT,
    lng FLOAT
);`;

cn.query(CREATE_TABLE_QUERY, (error, results, fields) => {

    if(error) {
        console.log(error);
    }

    console.log(results);
    loadData(cn);
    
});


function loadData(cn) {
    var query = `INSERT INTO restaurants (id, rating, name, site, email, phone, street, city, state, lat, lng ) VALUES \n`;

    fs.createReadStream(path.resolve(__dirname,'../database','restaurantes.csv'))
    .pipe(csv.parse({ headers: true }))
    .on('error', e => console.error(e))
    .on('data', row => {

        query += `('${row.id}',${row.rating},'${row.name}','${row.site}','${row.email}','${row.phone}','${row.street}','${row.city}','${row.state}',${row.lat},${row.lng}),`;
    
    })
    .on('end', () => {

        query = query.slice(0, -1);
        query += ';';
        insertData(cn, query);

    });
}

function insertData(cn, query) {

    cn.query(query, (error, results, fields) => {

        if(error) {
            console.log(error);
        }
    
        console.log(results);
        process.exit();

    });

}