import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import bodyParser from 'body-parser';
import router from './routes/restaurants'

const app = express();

// Middlewares
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(express.json());

// Routes
app.use('/api', router);

app.use('*', (req, res, next) => {
    res.status(404).send('Resources not found - Melp');
})

export default app;