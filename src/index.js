import 'dotenv/config';
import app from './app';
import http from 'http'

const serverHttp = http.createServer(app);
const PORT = process.env.PORT;
const HOST = process.env.HOST;

const main = async() => {
    try {
        await serverHttp.listen(PORT, HOST, () => console.log(`Server running on port ${PORT}`))
    } catch (e) {
        console.log(e.message)
    }
}

main();